module Structures where

{-# LANGUAGE TemplateHaskell #-}

import Control.Lens
import System.MIDI

-- import Loop
-- import Shell
-- import Transmit

data Prompt = Prompt {
  _bpm         :: Int,
  _track       :: Track,
  _isOnCC      :: Bool,
  _isPlaying   :: Bool,
  _isRecording :: Bool
}

--data PromptCons = PC 

data Track = Track {
  _channel     :: Int,
  _len         :: Int,
  _isMuted     :: Bool,
  _isSoloed    :: Bool
}

-- data Sounds = Sounds {
--   _
-- }

-- (current track, play?, rec?, list of tracks)
data SoundStatus = SoundStatus {
  _soundChannel :: Int,
  _soundPlaying :: Bool,
  _soundRec     :: Bool,
  _soundTracks  :: [SoundTrack]
}  
-- (mute? , list of beats)
data SoundTrack = SoundTrack {
  _soundMute :: Bool,
  _soundBeat :: [[MidiMessage]]
} 

-- lenses will be used to easily update a whole prompt
makeLenses ''Prompt
makeLenses ''Track
makeLenses ''SoundStatus
makeLenses ''SoundTrack

{-

bpm :: Lens' Prompt Int
bpm = lens _bpm (\prompt v -> prompt { _bpm = v })
track :: Lens' Prompt Track
track = lens _track (\prompt v -> prompt { _track = v })
isOnCC :: Lens' Prompt Bool
isOnCC = lens _isOnCC (\prompt v -> prompt { _isOnCC = v })
isPlaying :: Lens' Prompt Bool
isPlaying = lens _isPlaying (\prompt v -> prompt { _isPlaying = v })
isRecording :: Lens' Prompt Bool
isRecording = lens _isRecording (\prompt v -> prompt { _isRecording = v })

channel :: Lens' Track Int
channel = lens _channel (\prompt v -> prompt { _channel = v })
len :: Lens' Track Int
len = lens _len (\prompt v -> prompt { _len = v })
isMuted :: Lens' Track Bool
isMuted = lens _isMuted (\prompt v -> prompt { _isMuted = v })
isSoloed :: Lens' Track Bool
isSoloed = lens _isSoloed (\prompt v -> prompt { _isSoloed = v }) 

-}

instance Eq Prompt where
  p1 == p2 = show p1 == show p2

instance Show Prompt where
  show p = spellIsPlaying ++ spellIsRecording ++ "On Track " ++ 
           (show $ view (track . channel) p) ++ " | Steps: " ++
           (show $ spellLen p) ++ "/16 @ " ++
           (show $ view bpm p) ++ " (1/4)BPM" ++ 
           spellIsMuted ++ spellIsSoloed ++
           spellIsOnCC where
    spellIsRecording 
      | view isRecording p == True = "\x1b[31mREC\x1b[0m | "
      | otherwise             = ""
    spellIsPlaying 
      | view isPlaying   p == True = "PLAYING | "
      | otherwise             = "PAUSED  | "
    spellLen = view (track . len)
    spellIsMuted
      | view (track . isMuted) p == True = " | M"
      | otherwise                    = ""
    spellIsSoloed
      | view (track . isSoloed) p == True = " | S"
      | otherwise                    = ""
    spellIsOnCC
      | view isOnCC p == True = " || CC >> "
      | otherwise        = " >> "

initPrompt = Prompt {_bpm = 120, _track = initTrack, _isOnCC = False, 
                     _isPlaying = False, _isRecording = False}

initTrack = Track {_channel = 1, _len = 0, _isMuted = False,
                   _isSoloed = False}
                   
initSoundStatus = SoundStatus {_soundChannel = 1, _soundPlaying = False, 
                  _soundRec = False, _soundTracks = take 16 $ repeat 
                  initSoundTrack}
                  
initSoundTrack = SoundTrack {_soundMute = False, _soundBeat = [[]]}