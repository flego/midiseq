module Shell where

-- import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.Trans.State.Lazy
import System.IO
-- import Sound.MIDI.IO
import System.MIDI.Utility
import System.MIDI
import Text.Read (readMaybe)
-- import Rainbow

import Structures

-- needed for state toggling
{-# LANGUAGE RankNTypes, PatternGuards #-}

firstPrompt :: Async a -> Prompt -> TVar SoundStatus -> IO ()
firstPrompt transmitter prompt loopCache = do
  -- swapTVar loopCache initPrompt
  putStr $ show initPrompt
  retrieve transmitter prompt loopCache

retrieve :: Async a -> Prompt -> TVar SoundStatus -> IO ()
retrieve transmitter prompt loopCache = do 
  com <- getLine
  -- putStrLn $ "You said: " ++ com
  newPrompt <- select com prompt
  -- tryPutMVar loopCache newPrompt -- fill TVar here for other threads
  case newPrompt of
    Nothing -> return ()
    Just p  -> retrieve transmitter p loopCache
  
select :: String -> Prompt -> IO (Maybe Prompt)
select "c"  p = do 
  res <- togglePromptElem p isOnCC 
  putStr $ show res
  return $ Just res
select "p"  p = do 
  res <- togglePromptElem p isPlaying 
  putStr $ show res
  return $ Just res
select "r"  p = do 
  res <- togglePromptElem p isRecording
  putStr $ show res
  return $ Just res
select ('t':' ':s)  p = do 
  res <- setTrack s p 
  putStr $ show res
  return $ Just res
select ('b':' ':s)  p = do 
  let oldT = view bpm p
  res <- setBPM s p oldT
  putStr $ show res
  return $ Just res
select "m"  p = do 
  res <- toggleTrackElem p isMuted 
  putStr $ show res
  return $ Just res
select "s"  p = do 
  res <- toggleTrackElem p isSoloed 
  putStr $ show res
  return $ Just res
select "q" p = return Nothing
select "play"  p  = select "p" p
select "pause" p  = select "p" p
select "rec" p    = select "r" p
select "record" p = select "r" p
select ('t':'r':'a':'c':'k':' ':s) p = select ('t':' ':s) p
select ('b':'p':'m':' ':s) p         = select ('b':' ':s) p
select "mute" p   = select "m" p
select "solo" p   = select "s" p
select "quit" p   = select "q" p
select _    p = do
  putStr $ show p 
  return $ Just p

togglePromptElem :: Prompt -> Lens' Prompt Bool -> IO Prompt
togglePromptElem p le = do
  let newp = not $ view le p
  return $ set le newp p
  
toggleTrackElem :: Prompt -> Lens' Track Bool -> IO Prompt
toggleTrackElem p le = do
  let newp = not $ view (track . le) p
  return $ set (track . le) newp p

setTrack :: String -> Prompt -> IO Prompt 
setTrack s p = return $ set (track . channel) (parseMaybeOrDefault (readMaybe s) 1) p 
  
setBPM :: String -> Prompt -> Int -> IO Prompt
setBPM s p oldT = return $ set bpm (parseMaybeOrDefault (readMaybe s) oldT) p  

-- catch possible read exception
parseMaybeOrDefault :: (Maybe Int) -> Int -> Int
parseMaybeOrDefault (Just i) def = i
parseMaybeOrDefault Nothing  def = def


