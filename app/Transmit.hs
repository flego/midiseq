module Transmit where

import Control.Concurrent
import Control.Concurrent.Async
-- import Control.Concurrent.STM
import Control.Monad
import System.IO
-- import Sound.MIDI.IO
-- import System.MIDI.Utility
import System.MIDI

getMidiMessages :: Connection -> IO [MidiMessage] 
getMidiMessages connIn = do
  allEvs <- getEvents connIn
  let evs = filter isNote allEvs
      mess = map takeMessage evs
--  mapM_ print mess
  return mess
  
isNote :: MidiEvent -> Bool
isNote (MidiEvent _ (MidiMessage _ _)) = True
isNote _                               = False

takeMessage :: MidiEvent -> MidiMessage
takeMessage (MidiEvent _ mes) = mes

toChannel :: Int -> MidiMessage -> MidiMessage
toChannel n mes@(MidiMessage _ note)
  | n > 0 && n < 17 = MidiMessage n note
  | otherwise       = mes
toChannel _ other = other 

-- channel number   
transmit :: Connection -> Connection -> Int -> IO () 
transmit connIn connOut t = forever $ do
  mess <- getMidiMessages connIn
  mapM_ (send connOut . toChannel t) mess
  threadDelay 5000
