module Main where

{-# LANGUAGE TemplateHaskell #-}

import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Control.Lens
import Control.Monad
import Control.Monad.Trans.State.Lazy
import System.IO
-- import Sound.MIDI.IO
import System.MIDI.Utility
import System.MIDI

import Structures
import Loop
import Shell
import Transmit


quit :: Async a -> IO ()
quit = cancel

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stdin  NoBuffering

  putStrLn ""
  putStrLn "\x1b[33m  • ▌ ▄ ·. ▪  ·▄▄▄▄  ▪  .▄▄ · ▄▄▄ ..▄▄▄ \x1b[0m"
  putStrLn "\x1b[33m·██ ▐███▪██ ██▪ ██ ██ ▐█ ▀. ▀▄.▀·▐▀•▀█  \x1b[0m"
  putStrLn "\x1b[33m▐█ ▌▐▌▐█·▐█·▐█· ▐█▌▐█·▄▀▀▀█▄▐▀▀▪▄█▌·.█▌ \x1b[0m"
  putStrLn "\x1b[33m██ ██▌▐█▌▐█▌██. ██ ▐█▌▐█▄▪▐█▐█▄▄▌▐█▪▄█· \x1b[0m"
  putStrLn "\x1b[33m▀▀  █▪▀▀▀▀▀▀▀▀▀▀▀• ▀▀▀ ▀▀▀▀  ▀▀▀ ·▀▀█.  \x1b[0m"
  putStrLn ""
  putStrLn "v0.0.9"
  putStrLn ""
  
  src <- selectInputDevice "please select an input device" Nothing
  -- putStrLn "got input"
  
  dst <- selectOutputDevice "please select an output device" Nothing
  --  putStrLn "got output"
  
  connIn  <- openSource src Nothing  
  connOut <- openDestination dst
  
  putStrLn "starting transmission..."
  putStrLn "help not yet implemented"
  putStrLn "try one of the following commands:"
  putStrLn "p, r, t <1-16>, m, s"
  putStrLn "or press q to quit"
  mapM_ start [connIn, connOut] 
 
  -- TODO : loopCache to STM
  loopCache <- newTVarIO initSoundStatus
  
  transmitter <- async $ transmit   connIn  connOut 1
  looper      <- async $ updateLoop connOut loopCache 
  firstPrompt transmitter initPrompt loopCache
  
  mapM_ quit  [transmitter, looper]
  mapM_ close [connIn, connOut]    
  putStrLn "connections closed"
  putStrLn "bye!"
