module Loop where 
  
import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Concurrent.STM
import Control.Lens
import Control.Monad
-- import Control.Monad.Trans.State.Lazy
import System.IO
import System.MIDI.Utility
import System.MIDI

import Structures
-- import Shell

-- For contiunous playback we are in need of a clock
-- that will tell us how many microseconds to wait before
-- the next step has to be triggered. This value will be
-- supplied by function convT, which in turn uses the bpm 
-- record from our Prompt .
-- 
-- Beats Per Minute usually refer to quarter notes while
-- those on drum machines and sequencers most commonly are
-- measured in sixteenths.

convT :: Int -> Int 
convT bpm = (60 * 1000000) `quot` (4 * bpm) -- bpm to delta t

-- TODO : data type for loop
--      output   isPlaying?   bpm  channel  mute?  messages
loop :: Connection -> Bool -> Int -> [(Int, Bool, [MidiMessage])] -> IO ()
loop connOut True bpm [] = do 
  threadDelay $ convT bpm
  loop connOut True bpm []
loop connOut True bpm (step:tape) = do
  -- let toOutput = gen connOut step
  gen connOut step
  threadDelay $ convT bpm
loop _ _ _ _ = return ()
  
gen :: Connection -> (Int, Bool, [MidiMessage]) -> IO ()
-- gen connOut (channel, isMuted, [])       = return ()
gen connOut (channel, isMuted, mess) = do
  if not isMuted then mapM_ (send connOut) mess else return ()
    
    
initLoop :: Connection -> TVar SoundStatus -> IO ()
initLoop connOut sounds = do
  undefined
  -- updateLoop 
  -- wait
  -- playloop

updateLoop :: Connection -> TVar SoundStatus -> IO ()
updateLoop connOut sound = do
  undefined
  -- readTVar
  -- b <- getBPM
  -- getNotes
  -- playNotes
  -- threadDelay