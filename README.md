MIDIseq : README
----------------

This is a prototype of a MIDI sequencer. It is not fully functional yet, 
but can be used for code inspecting purposes. Make sure to have a supported
MIDI Framework on your computer installed. Windows/macos should work right 
now, Linux support is coming soon! 

Feel free to contact me if you have questions.