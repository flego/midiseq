main :: IO ()
main = putStrLn "Test suite not yet implemented"

{-
--  beeper <- forkIO $! testTransmit connOut

testBeep :: Connection -> IO () -- will be using another send function
testBeep conn = forever $ do
  send conn beep
  threadDelay 1000000
  send conn beepOff
  threadDelay 1000000

beep :: MidiMessage
beep = MidiMessage 1 (NoteOn 77 92)
beepOff :: MidiMessage
beepOff = MidiMessage 1 (NoteOff 77 92)

chunkTest = putChunkLn $ chunk "Blue on 8, red on 256" &
            fore red
            
pr :: Bool -> IO ()
pr new = putStrLn $ "new: " ++ show new

-- same as not
invert :: Bool -> Bool
invert False = True
invert True  = False
-}
