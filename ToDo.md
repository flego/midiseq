# ToDo

* [x]Create new branch with Structures.hs
* [ ]firstPrompt move to Structures

* [ ]parallel: refactor this list
* [ ]Text.Printf for formatting
* [ ]ANSI colored terminal
* [ ]replace MVars with State completely
* [x]eliminate State
* [x]transmit: Pass Int through to toChannel
* [ ]Exception for read :: Int
* [ ]makeLenses TH in separate file or omit altogether
* [x]forkIO does not need $!
* [ ]instance Eq Prompt
* [x]fix convT
* [ ]retrieve: quit without isNext, Maybe?
* [x]execute quit in Main
* [ ]Shell: single State monad
* [ ]loop: correct use of MVar
* [ ]loop: separate thread for each track
* [ ]replace MVar with STM?
* [ ]visualize beats per track